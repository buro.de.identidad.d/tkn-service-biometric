package com.teknei.bid.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(of = {"id", "serial"})
public class BiometricCaptureRequestIdentDTO implements Serializable {

    private Long id;
    private String serial;
    private String username;
    private String type;

}