package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BiometricDetailDTO implements Serializable{

    private boolean ll;
    private boolean lr;
    private boolean lm;
    private boolean li;
    private boolean lt;
    private boolean rl;
    private boolean rr;
    private boolean rm;
    private boolean ri;
    private boolean rt;

}