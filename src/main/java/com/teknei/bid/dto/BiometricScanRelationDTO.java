package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BiometricScanRelationDTO implements Serializable {

    private String scanId;
    private String biometricId;
    private Long idCustomer;

}