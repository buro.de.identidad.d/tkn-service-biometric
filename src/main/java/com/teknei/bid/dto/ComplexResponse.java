package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ComplexResponse implements Serializable {

    private String id;
    private String rightIndex;
    private String leftIndex;

}