package com.teknei.bid.util.biom;

import com.teknei.bid.dto.BiometricCaptureRequestIdentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class BiometricStatusSingleton {

    private Map<String, Integer> statusMap;
    private Map<BiometricCaptureRequestIdentDTO, Integer> statusAuthMap;
    private Map<BiometricCaptureRequestIdentDTO, Integer> statusSignMap;
    private Map<BiometricCaptureRequestIdentDTO, Integer> statusConfirmation;
    private Map<BiometricCaptureRequestIdentDTO, Integer> statusConfirmationSign;
    private Map<String, String> serialOperationMap;
    private static BiometricStatusSingleton instance = new BiometricStatusSingleton();

    private static final Logger log = LoggerFactory.getLogger(BiometricStatusSingleton.class);

    private BiometricStatusSingleton() {
        statusMap = new HashMap<>();
        statusAuthMap = new HashMap<>();
        statusSignMap = new HashMap<>();
        statusConfirmation = new HashMap<>();
        statusConfirmationSign = new HashMap<>();
        serialOperationMap = new HashMap<>();
    }

    public void addRelationSerialId(String serial, String id){
        serialOperationMap.put(serial, id);
    }

    public String getIdFromSerial(String serial){
        return serialOperationMap.get(serial);
    }

    public void initCapture(String serialNumber) {
        statusMap.put(serialNumber, 1);
    }

    public void endCapture(String serialNumber) {
        statusMap.put(serialNumber, 0);
    }

    public Integer getStatusFor(String serialNumber) {
        Boolean found = statusMap.containsKey(serialNumber);
        if (found) {
            Integer status = statusMap.get(serialNumber);
            return status;
        }
        return 0;
    }

    public void initCaptureAuth(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        statusAuthMap.put(requestIdentDTO, 1);
    }

    public void initCaptureSign(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        statusSignMap.put(requestIdentDTO, 1);
    }

    public Integer endCaptureAuth(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        statusAuthMap.remove(requestIdentDTO);
        return 0;
    }

    public Integer endCaptureSign(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        statusSignMap.remove(requestIdentDTO);
        return 0;
    }

    public BiometricCaptureRequestIdentDTO findCaptureDataFor(String serial) {
        for (BiometricCaptureRequestIdentDTO key : statusAuthMap.keySet()) {
            if (key.getSerial().trim().equals(serial.trim())) {
                return key;
            }
        }
        return null;
    }

    public BiometricCaptureRequestIdentDTO findCaptureSignDataFor(String serial) {
        for (BiometricCaptureRequestIdentDTO key : statusSignMap.keySet()) {
            if (key.getSerial().trim().equals(serial.trim())) {
                return key;
            }
        }
        return null;
    }

    public Integer getStatusForAuth(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Boolean found = statusAuthMap.containsKey(requestIdentDTO);
        if (found) {
            Integer status = statusAuthMap.get(requestIdentDTO);
            return status;
        }
        return 0;
    }

    public Integer getStatusForSign(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Boolean found = statusSignMap.containsKey(requestIdentDTO);
        if (found) {
            Integer status = statusSignMap.get(requestIdentDTO);
            return status;
        }
        return 0;
    }

    public Integer confirmStatus(BiometricCaptureRequestIdentDTO requestIdentDTO, Integer status) {
        statusConfirmation.put(requestIdentDTO, status);
        return status;
    }


    public Integer confirmStatusSign(BiometricCaptureRequestIdentDTO requestIdentDTO, Integer status) {
        statusConfirmationSign.put(requestIdentDTO, status);
        return status;
    }

    public Integer queryStatus(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Boolean found = statusConfirmation.containsKey(requestIdentDTO);
        if (found) {
            Integer statusFound = statusConfirmation.get(requestIdentDTO);
            statusConfirmation.put(requestIdentDTO, 0);
            return statusFound;
        }
        return 0;
    }


    public Integer queryStatusSign(BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Boolean found = statusConfirmationSign.containsKey(requestIdentDTO);
        if (found) {
            Integer statusFound = statusConfirmationSign.get(requestIdentDTO);
            statusConfirmationSign.put(requestIdentDTO, 0);
            return statusFound;
        }
        return 0;
    }

    public static BiometricStatusSingleton getInstance() {
        return instance;
    }

}