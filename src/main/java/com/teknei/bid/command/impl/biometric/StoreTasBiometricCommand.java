package com.teknei.bid.command.impl.biometric;

import com.teknei.bid.command.*;
import com.teknei.bid.common.utils.LogUtil;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Component
public class StoreTasBiometricCommand implements Command {

    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.fingers}")
    private String tasFingers;
    @Value("${tkn.tas.singleFinger}")
    private String tasSingleFinger;
    
    @Value("${biometric.engine}")
    private String engine;
    private static final String KARALUNDI = "karalundi";  
    
    @Autowired
    private TasManager tasManager;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidClieRepository clieRepository;

    private static final Logger log = LoggerFactory.getLogger(StoreTasBiometricCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info("INFO: StoreTasBiometricCommand.execute:  DocumentId: "+request.getDocumentId()+", ScanId:"+request.getScanId()+", Id:"+request.getId());
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setDocumentId(request.getDocumentId());
        commandResponse.setId(request.getId());
        commandResponse.setScanId(request.getScanId());
        String responseDocumentManager = null;
        if (request.getRequestType().equals(RequestType.BIOM_SLAPS_REQUEST)) {
            commandResponse.setStatus(Status.BIOM_TAS_OK);
            return commandResponse;
        }
        try {
            responseDocumentManager = addMinuciasDocumentManager(request.getData(), request.getDocumentId(), request.getScanId(), request.getId());
        } catch (Exception e) {
            log.error("ERROR: StoreTasBiometricCommand.execute:Error adding data to TAS with message: {}", e.getMessage());
            responseDocumentManager = null;
        }
        if (responseDocumentManager == null) {
            commandResponse.setStatus(Status.BIOM_TAS_ERROR);
        } else {
            commandResponse.setStatus(Status.BIOM_TAS_OK);
        }
        return commandResponse;
    }

    public String addMinuciasDocumentManager(String jsonRequest, String idDocumentManager, String idScan, Long operationId) throws Exception {
    	
    	 JSONObject jsonObject = new JSONObject(jsonRequest);//TODO VALIDACION INDICES
         String lt = jsonObject.optString("lt", "");
         String li = jsonObject.optString("li", "");
         String lm = jsonObject.optString("lm", "");
         String lr = jsonObject.optString("lr", "");
         String ll = jsonObject.optString("ll", "");
         String rt = jsonObject.optString("rt", "");
         String ri = jsonObject.optString("ri", "");
         String rm = jsonObject.optString("rm", "");
         String rr = jsonObject.optString("rr", "");
         String rl = jsonObject.optString("rl", "");
         String comodin = "";
         log.info("INFO: StoreTasBiometricCommand.addMinuciasDocumentManager: "+LogUtil.logJsonObject(jsonObject));
         
        if(engine.equalsIgnoreCase(KARALUNDI)) {      
        	log.info("INFO: Karalundi");
        	comodin = !lt.isEmpty() ? addMinucias(lt, idDocumentManager, idScan, operationId,"lt").toString() : "";
    		comodin = !li.isEmpty() ? addMinucias(li, idDocumentManager, idScan, operationId,"li").toString() : "";
    		comodin = !lm.isEmpty() ? addMinucias(lm, idDocumentManager, idScan, operationId,"lm").toString() : "";
    		comodin = !lr.isEmpty() ? addMinucias(lr, idDocumentManager, idScan, operationId,"lr").toString() : "";
    		comodin = !ll.isEmpty() ? addMinucias(ll, idDocumentManager, idScan, operationId,"ll").toString() : "";
    		comodin = !rt.isEmpty() ? addMinucias(rt, idDocumentManager, idScan, operationId,"rt").toString() : "";
    		comodin = !ri.isEmpty() ? addMinucias(ri, idDocumentManager, idScan, operationId,"ri").toString() : "";
    		comodin = !rm.isEmpty() ? addMinucias(rm, idDocumentManager, idScan, operationId,"rm").toString() : "";
    		comodin = !rr.isEmpty() ? addMinucias(rr, idDocumentManager, idScan, operationId,"rr").toString() : "";
    		comodin = !rl.isEmpty() ? addMinucias(rl, idDocumentManager, idScan, operationId,"rl").toString() : "";
    		return comodin;
        }
       
        //Buscando alguna guella valida.
		comodin = lt.isEmpty() ? comodin : lt;
		comodin = li.isEmpty() ? comodin : li;
		comodin = lm.isEmpty() ? comodin : lm;
		comodin = lr.isEmpty() ? comodin : lr;
		comodin = ll.isEmpty() ? comodin : ll;
		comodin = rt.isEmpty() ? comodin : rt;
		comodin = ri.isEmpty() ? comodin : ri;
		comodin = rm.isEmpty() ? comodin : rm;
		comodin = rr.isEmpty() ? comodin : rr;
		comodin = rl.isEmpty() ? comodin : rl;
		
		//llenando indices
		li = li.isEmpty()? comodin: li;
		ri = ri.isEmpty()? comodin: ri;
		
        
        JSONObject object = addMinucias(li, ri, idDocumentManager, idScan, operationId);
        return object.toString();
    }

    
    public JSONObject addMinucias(String finger,  String idDocumentManager, String scanId, Long operationId,String fingerReference) throws Exception {    	
    	log.info("INFO: StoreTasBiometricCommand.addMinucias:  operationId:"+ operationId +" minucia:"+fingerReference);
        Map<String, String> docProperties = getMetadataMapAddress(scanId, operationId);
        JSONObject response = null;
        if (finger != null && !finger.isEmpty()) {
            byte[] bytes = Base64Utils.decodeFromString(finger);
            docProperties.put(tasSingleFinger, fingerReference);
            response = tasManager.addDocument(tasFingers, idDocumentManager, null, docProperties, bytes, "application/octet-stream", "Minucia_"+fingerReference+".wsq");
        }       
        return response;
    }

    public JSONObject addMinucias(String li, String ri, String idDocumentManager, String scanId, Long operationId) throws Exception {
    	log.info("INFO: StoreTasBiometricCommand.addMinucias:  operationId:"+ operationId );
        Map<String, String> docProperties = getMetadataMapAddress(scanId, operationId);
        JSONObject response = null;
        if (li != null && !li.isEmpty()) {
            byte[] bytes = Base64Utils.decodeFromString(li);
            docProperties.put(tasSingleFinger, "2I");
            response = tasManager.addDocument(tasFingers, idDocumentManager, null, docProperties, bytes, "application/octet-stream", "Minucia-DI.jpeg");
        }
        if (ri != null && !ri.isEmpty()) {
            byte[] bytes = Base64Utils.decodeFromString(li);
            docProperties.put(tasSingleFinger, "2D");
            response = tasManager.addDocument(tasFingers, idDocumentManager, null, docProperties, bytes, "application/octet-stream", "Minucia-DD.jpeg");
        }
        return response;
    }

    private Map<String, String> getMetadataMapAddress(String scanId, Long operationId) throws Exception {
    	log.info("INFO: StoreTasBiometricCommand.getMetadataMapAddress: operationId:"+ operationId );
        PersonData scanInfo = getPersonalDataFromScan(scanId, operationId);
        Map<String, String> docProperties = new HashMap<>();
        docProperties.put(tasName, scanInfo.getName());
        docProperties.put(tasSurname, scanInfo.getSurename());
        docProperties.put(tasLastname, scanInfo.getSurenameLast());
        docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        return docProperties;
    }


    public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception {
    	log.info("INFO: StoreTasBiometricCommand.getPersonalDataFromScan:  operationId:"+ operationId );
        PersonData personalData = new PersonData();
        BidClie bidClie = clieRepository.findOne(operationId);
        personalData.setPersonalNumber(String.valueOf(operationId));
        String name = bidClie.getNomClie() == null ? "" : bidClie.getNomClie();
        String surname = bidClie.getApePate() == null ? "" : bidClie.getApePate();
        String surnamelast = bidClie.getApeMate() == null ? "" : bidClie.getApeMate();
        String surnames = surname + " " + surnamelast;
        personalData.setName(name);
        personalData.setSurename(surname);
        personalData.setSurenameLast(surnamelast);
        personalData.setLastNames(surnames);
        return personalData;
    }
}
