package com.teknei.bid.command.impl.biometric;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieDact;
import com.teknei.bid.persistence.entities.BidClieRegProc;
import com.teknei.bid.persistence.entities.BidClieRegProcPK;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidDactRepository;
import com.teknei.bid.persistence.repository.BidRegProcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class PersistBiometricCommand implements Command {

    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidDactRepository bidDactRepository;

    private static final Logger log = LoggerFactory.getLogger(PersistBiometricCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
    	log.info("INFO: PersistBiometricCommand.execute: ScanId:"+ request.getScanId() 
        +", DocumentId:"+ request.getDocumentId() +", Id:"+request.getId()+", Username:"+request.getUsername().substring(0,7));
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setScanId(request.getScanId());
        commandResponse.setDocumentId(request.getDocumentId());
        commandResponse.setId(request.getId());
        try {
            addDB(request.getId(), request.getUsername().substring(0,7));
            commandResponse.setStatus(Status.BIOM_DB_OK);
        } catch (Exception e) {
            commandResponse.setStatus(Status.BIOM_DB_ERROR);
            log.error("ERROR: PersistBiometricCommand.execute:Error persisting data to db in biometric stage with message: {}", e.getMessage());
        }
        return commandResponse;
    }

    private void addDB(Long idCustomer, String username) {
        register(idCustomer, username);
        alterRecord(idCustomer, true, username);
    }

    private void register(Long idCustomer, String username) 
    {
    	log.info("INFO: PersistBiometricCommand.register:  idCustomer:"+ idCustomer );
        BidClieDact dact = new BidClieDact();
        dact.setIdClie(idCustomer);
        dact.setIdDact(idCustomer);
        dact.setUsrCrea(username);
        dact.setUsrOpeCrea(username);
        dact.setIdEsta(1);
        dact.setIdTipo(3);
        dact.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidDactRepository.save(dact);
    }

    private void alterRecord(Long operationId, boolean created, String username) {
    	log.info("INFO: PersistBiometricCommand.alterRecord:  operationId:"+ operationId );
        BidClieCurp curp = bidCurpRepository.findTopByIdClie(operationId);
        BidClieRegProcPK pk = new BidClieRegProcPK();
        pk.setIdClie(operationId);
        pk.setCurp(curp.getCurp());
        BidClieRegProc proc = bidRegProcRepository.findOne(pk);
        proc.setRegDact(created);
        proc.setFchRegDact(new Timestamp(System.currentTimeMillis()));
        proc.setUsrOpeCrea(username);
        proc.setUsrCrea(username);
        proc.setIdEsta(1);
        proc.setIdTipo(3);
        proc.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidRegProcRepository.save(proc);
    }
}
